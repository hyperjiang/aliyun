package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"

	"github.com/aliyun/alibaba-cloud-sdk-go/services/domain"
	"github.com/joho/godotenv"
)

// Domain -
type Domain struct {
	Name  string `json:"name"`
	Avail bool   `json:"avail"`
}

func load(f string) []*Domain {
	var domains []*Domain
	data, err := ioutil.ReadFile("./" + f)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(data, &domains)
	if err != nil {
		panic(err)
	}
	return domains
}

func main() {

	var jsonFile = flag.String("f", "domain.json", "domain list file")
	var r = flag.Int("r", 0, "remove unavailable domains")
	flag.Parse()

	// remove unavailable domains
	if *r > 0 {
		domains := load(*jsonFile)
		var domains2 []*Domain
		for _, n := range domains {
			if n.Avail == true {
				domains2 = append(domains2, n)
			}
		}
		j, _ := json.Marshal(domains2)
		ioutil.WriteFile("./"+*jsonFile, j, os.ModePerm)
		return
	}

	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	regionID := os.Getenv("region-id")
	accessKeyID := os.Getenv("access-key-id")
	accessKeySecret := os.Getenv("access-key-secret")

	client, err := domain.NewClientWithAccessKey(
		regionID,
		accessKeyID,
		accessKeySecret,
	)
	if err != nil {
		panic(err)
	}

	request := domain.CreateCheckDomainRequest()

	domains := load(*jsonFile)
	for _, n := range domains {
		if !n.Avail {
			continue
		}
		request.DomainName = n.Name + ".com"
		fmt.Println("checking " + request.DomainName)

		response, err := client.CheckDomain(request)
		if err != nil {
			panic(err)
		}
		fmt.Println(response)

		if response.Avail == "0" {
			n.Avail = false
			j, _ := json.Marshal(domains)
			ioutil.WriteFile("./"+*jsonFile, j, os.ModePerm)
		}

		time.Sleep(time.Second)
	}
}
